# What is OCI?
    https://www.opencontainers.org/

# Universal Base Image:
    https://www.redhat.com/en/blog/introducing-red-hat-universal-base-image

# oc commands for newbies:
    https://blog.openshift.com/oc-command-newbies/

# Learn YAML:
    https://learnxinyminutes.com/docs/yaml/

# Automation Language Detection when using the s2i process:
    https://docs.openshift.com/container-platform/4.3/applications/application_life_cycle_management/creating-applications-using-cli.html#language-detection

# The basics of application creation:
    https://access.redhat.com/documentation/en-us/openshift_container_platform/3.11/html-single/developer_guide/application-life-cycle-management#dev-guide-new-app

# Using builder images:
    https://access.redhat.com/documentation/en-us/openshift_container_platform/3.11/html-single/using_images/index

# ImageStream benefits
    https://developers.redhat.com/blog/2019/09/20/using-red-hat-openshift-image-streams-with-kubernetes-deployments/

# Deployments vs DeploymentConfigs:
    https://docs.openshift.com/container-platform/4.1/applications/deployments/what-deployments-are.html#deployments-comparing-deploymentconfigs_what-deployments-are

# Building images
    https://access.redhat.com/documentation/en-us/openshift_container_platform/3.11/html-single/creating_images/index

# Troubleshooting using oc inject:
    https://developers.redhat.com/blog/2020/01/15/installing-debugging-tools-into-a-red-hat-openshift-container-with-oc-inject/

# OpenShift applications ignoring USER instruction and mitigation:
    https://access.redhat.com/documentation/en-us/openshift_container_platform/3.11/html/creating_images/creating-images-guidelines#general-container-image-guidelines

    https://access.redhat.com/documentation/en-us/openshift_container_platform/3.11/html-single/creating_images/index

# Pod scheduling:
    https://access.redhat.com/documentation/en-us/openshift_container_platform/4.2/html-single/nodes/index#controlling-pod-placement-onto-nodes-scheduling

    https://www.ansible.com/blog/how-useful-is-ansible-in-a-cloud-native-kubernetes-environment

# Taints and tolerances:
    https://www.openshift.com/blog/taints-expectations-vs.-reality

# Good resource for templates:
    https://docs.openshift.com/container-platform/4.5/openshift_images/using-templates.html

# How to Create an S2I Builder Image:
    https://www.openshift.com/blog/create-s2i-builder-image

# How to override S2I builder scripts:
    https://www.openshift.com/blog/override-s2i-builder-scripts

# s2i detailed documentation and code:
    https://github.com/openshift/source-to-image/blob/master/docs/cli.md

# Red Hat container image and host guide: Application portability
    https://www.redhat.com/en/resources/container-image-host-guide-technology-detail
